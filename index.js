import WebSocket from 'ws';
import cleanUpDeeply from './clean-up-deeply.js';
import merge from 'lodash/merge.js';
import dotenv from 'dotenv';

// ---------------------------------------------------------------------- //

dotenv.config();

// ---------------------------------------------------------------------- //

let globalGameList = null;
let globalGames = {};
let gameList = null;
let games = {};

// ---------------------------------------------------------------------- //

const socket = new WebSocket(`wss://api.topfeeds.tech/v1`);

socket.send = ((send) => {
	return function (message) {
		let {
			type = null,
			data = null,
			id = null,
			relatedId = null,
			error = null,
		} = message;

		let params = [
			type,
			data,
			id,
			relatedId,
			error,
		];

		params = params.reduceRight((params, param) => {
			if (param !== null || params.length > 0) {
				params.unshift(param);
			}

			return params;
		}, []);

		message = JSON.stringify(params);
		console.info(`>> ${message}`);
		return send.call(this, message);
	};
})(socket.send);

socket.on('open', () => {
	socket.nextOutgoingMessageId = 1;
	console.info(`WebSocket: open.`);

	socket.send({
		id: socket.nextOutgoingMessageId++,
		type: 'authorize',
		data: {
			apiKey: process.env.API_KEY,
		},
	});
});

socket.on('message', (message) => {
	message = message.toString();
	console.info(`<<`, message);

	try {
		message = ((type = null, data = null, id = null, relatedId = null, error = null) => {
			return {
				type,
				data,
				id,
				relatedId,
				error,
			}
		})(...JSON.parse(message));
	} catch (error) {
		console.error(error);
		return;
	}

	if (message.error) {
		console.error(message.error);
		return;
	}

	let messageTypeMatch = null;

	if (message.type === 'authorized') {
		// Subscribing to GameList

		socket.send({
			id: socket.nextOutgoingMessageId++,
			type: 'gameList/subscribe',
			data: {},
		});

		// Subscribing to GlobalGameList

		// socket.send({
		// 	id: socket.nextOutgoingMessageId++,
		// 	type: 'globalGameList/subscribe',
		// 	data: {
		// 		minBookies: 1,
		// 	},
		// });

		return;
	}

	// GameList
	// ----------------------------------------------------------------------

	if (message.type === 'gameList/subscribed') {
		// console.info(`Subscribed to GameList.`);
		return;
	}

	if (messageTypeMatch = message.type.match(/^gameList\/(created|updated|deleted)$/)) {
		if (message.data === '\x00') {
			gameList = null;
		} else {
			if (gameList) {
				merge(gameList, message.data);
				cleanUpDeeply(gameList);
			} else {
				gameList = message.data;
			}
		}

		syncGameSubscriptions();
		return;
	}

	if (messageTypeMatch = message.type.match(/^game:([0-9]+)\/(created|updated|deleted)/)) {
		let gameId = Number(messageTypeMatch[1]);

		if (message.data === '\x00') {
			delete games[gameId];
		} else {
			let game = games[gameId] || null;

			if (game) {
				merge(game, message.data);
				cleanUpDeeply(game);
			} else {
				game = games[gameId] = message.data;
			}
		}
	}

	// GlobalGameList
	// ----------------------------------------------------------------------

	if (message.type === 'globalGameList/subscribed') {
		console.info(`Successfuly subscribed to GlobalGameList.`);
		return;
	}

	if (messageTypeMatch = message.type.match(/^globalGameList\/(created|updated|deleted)$/)) {
		if (message.data === '\x00') {
			globalGameList = null;
		} else {
			if (globalGameList) {
				merge(globalGameList, message.data);
				cleanUpDeeply(globalGameList);
			} else {
				globalGameList = message.data;
			}
		}

		syncGlobalGameSubscriptions();
		return;
	}

	if (messageTypeMatch = message.type.match(/^globalGame:([0-9]+)\/(created|updated|deleted)/)) {
		let globalGameId = Number(messageTypeMatch[1]);

		if (message.data === '\x00') {
			delete globalGames[globalGameId];
		} else {
			let globalGame = globalGames[globalGameId] || null;

			if (globalGame) {
				merge(globalGame, message.data);
				cleanUpDeeply(globalGame);
			} else {
				globalGame = globalGames[globalGameId] = message.data;
			}
		}
	}
});

socket.on('error', (error) => {
	console.error(error);
});

socket.on('close', () => {
	console.info('WebSocket: closed.');
	process.exit(0);
});

// ---------------------------------------------------------------------- //

function syncGlobalGameSubscriptions() {
	for (let globalGameId of Object.keys(globalGameList || {}).map(Number)) {
		if (globalGames[globalGameId] === undefined) {
			globalGames[globalGameId] = null;

			socket.send({
				id: socket.nextOutgoingMessageId++,
				type: `globalGame:${globalGameId}/subscribe`,
			});
		}
	}
}

function syncGameSubscriptions() {
	for (let gameId of Object.keys(gameList || {}).map(Number)) {
		if (games[gameId] === undefined) {
			games[gameId] = null;

			socket.send({
				id: socket.nextOutgoingMessageId++,
				type: `game:${gameId}/subscribe`,
			});
		}
	}
}
