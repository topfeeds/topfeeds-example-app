# TopFeeds Example App

Before running the application, install dependencies by the following command:

`npm install`

After this, create a file contains all environment variables called `.env`
by coping it from the example file `.env.example`:

`cp .env.example .env`

Then, set up the right value for `API_KEY` variable,
and then run the application by the following command:

`npm run start`

The application subscribes to the list of all Games/GlobalGames. After successful subscription to the specific list
it subscribes to each object from the list separately and gets all data for this object.
